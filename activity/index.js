// This function will check if the number input or passed as an argument is an odd or even number.

function oddEvenChecker(num){
	if(typeof num === "number"){
		if(num % 2 == 0){
		console.log("The number is even.");
		}else {
			console.log("The number is odd.");
		}
	}else{
		alert("Invalid input.");
	}
};

oddEvenChecker(4);

// This function will check if the number input or passed as an argument is over or is less than the recommended budget.

function budgetChecker(num1){
	if(typeof num1 === "number"){
		if(num1 > 40000){
			console.log("You are over the budget.");
		}else{
			console.log("You have resources left.");
		}
	}else{
		alert("Invalid input.");
	}

};

budgetChecker("r");