/*Assignment Operators (=) - It allows us to assign a value to a
  variable

*/

let variable = "Initialize variable";

/*Mathematical Operators - addition (+) - subtraction
  (-) - mutliplication(*) - division (/) - modulo (%)

	Whenever you have used a mathematical operator, the value is
	returned. It is only up to us if we save that returned value. 
*/

let num1 = 5; 
let num2 = 10; 
let num3 = 4; 
let num4 = 40;

/*assignment operators allows us to assign the result of the operation to the value of the left operand. it allows us to save the result of the mathematical operation to the left operand. */

// Addition assignment operator (+=):
//Left operand is the variable or value of the left side of the operator
//right operand is the variable of the right side of the operator
//num1 = num1 + num4 -> re-assigned the value of num1 with the result of the num1 + num4
num1 += num4; 
console.log(num1); //45

num1 += num1;
console.log(num1);

num1 += 55;
console.log(num1); // -> previous right should not be affected/ re-assigned

let string1 = "Boston";
let string2 = "Celtics";

string1 += string2;
console.log(string1); //"BostonCeltics" - is the result in the concatenation ebtween 2 strings and saves the results in the left operand value.
console.log(string2); //"Celtics" - the right operand variable is not affected/re-assigned

// console.log(15 += num1); - produces an error because we do not use addition assignment operator when the left operand is just data. 


//Subtraction assignment operator (-=)
/*
	
*/

num1 -= num2;
console.log(num1); //135 - the result of subtraction was then re-assigned to our left operand

num1 -= num4;
console.log(num1);

num1 -= 10;
console.log(num1);

num1 -= string1;
console.log(num1); // NaN - because string1 is an alphanumeric string


// Multiplication assignment Operator
num2 *= num3;
console.log(num2); //40 - is the result of the multiplication was then re-assigned to our left operand.

num2 *= 5;
console.log(num2); // 200 -> 40 * 5

// Division assignment Operator
num4 /= num3;
console.log(num4); // 40/4 = 10 -  the result of the division was then re-assigned to the left operand

num4 /=2;
console.log(num4);


// Mathematical Operations - we follow MDAS (Multiplication, Division, Addition, Subtraction)

	let mdasResult = 1+2-3*4/5;
	console.log(mdasResult);
	/*
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/


// PEMDAS - Paranthesis, Exponents, Multiplication, Division, Addition, Subtraction

	let pemdasResult = 1 + (2-3)*(4/5);
	console.log(pemdasResult);
	/*
		1. 4/5 = 0.8
		2. 2-3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.2
	*/


// Increment and Decrement Operators
	/*
		Increment and decrement is adding or subtracting 1 from the variable and re-assigning the new value to the variable where the increment or decrement was used.

		2 types of incrementation
			- Pre-fix
			- Post-fix
	*/

	let z = 1;

	// Pre-fix Incrementation

	++z; // z = z + 1
	console.log(z); // 2 -> the value of z was added with 1 and is imeediately returned.

	// Post-fix incrementation

	z++;
	console.log(z);// 3 -> the value of z was added with 1
	console.log(z++); // 3 -> with post incrementation, the previous value of the variable is returned first before the actual incrementation.
	console.log(z++); // 4 -> new value is now returned

	// Pre-fix vs Post-fix Incrementation
	console.log(++z);// 6 -> the new vlaue is returned immedaitely in pre-fix

	console.log(z++);// 6 -> previous value was returned first because we have the post-fix incrementation
	console.log(z++);// 7 -> the new vlaue is now returned


	// Pre-fix and Post-fix Decrementation
	console.log(--z); //6 - with pre-fix decrementation the resukt if subtraction by 1 is returned immediately
	console.log(z--); //6 - with post-fix decrementation the result of the subtraction by 1 is NOT immediately returned, instead the previous value is returned.
	console.log(z);// 5 - the new value is now returned


/*Comparison Operators
	- are used to compare the values of the left and right operands
	- return boolean (T/F, Y/N)

	
*/

	// 1. Equality operator or loose eqauality operator (==) 
	console.log(1 == 1); // returns True


	// we can also save the result of a comparison into a variable
	let isSame = 55 == 55;
	console.log(isSame);// true

	console.log(1 == '1'); // true - in loose equality operator the priority is the sameness of the value because with loose equality operator, forced coercion is done before the comparison.

	// Type coercion/forced conversion - JS forcible chnages the data type of the operands

	console.log(0 == false); //true - with forced coercion, false was converted into a number and the equivalent of false into a number is 0.

	console.log(1 == true); //true - because the equivalent of true into a number is 1.

	console.log('1' == 1); //true

	console.log(true == "true");// false - true coerced into 1 - true coerced into a number results into NaN, so therefore, 1 is not equal to NaN.

	console.log("false" == false); //false - false coerced into 0 - string false coerced into a number results into NaN, so therefore, 0 is not equal to NaN.

	/*
		With loose equality comparison operator (==), values are compared and type, if operands do not have the same type, will be forced coerced/type coerced before the comparison of values.

		If either operand is a number or a boolean, the operands are converted into numbers.
	*/


	console.log(true == '1'); //true - true coerced into a number = 1, "1" converted into a number = 1 - 1 equals 1
	console.log("0" == false); //true


	// 2. Strict Equality Operator (===) - checks both VALUE and TYPE

	console.log(true === "1");// false -  because the operands have the same value but different types.

	console.log(1 === "1"); // false

	console.log("John" === "John"); //true - same value and type

	// Inequality Operators (!=)
		// 1. Loose Inequality Operators

		/*
			Checks whether the operands are NOT equal and/or have different values.

			Will do type coercion if the operands have different types:
		*/


		console.log("1" != 1);// false

		/*false - both operands were converted to numbers
			1. "1" converted into number is 1.
			2. 1 converted into number is 1.
			3. 1 equals 1 is not equal

		*/

		console.log("James" != "John"); //true
		/*
			true - "James" is not equal to "John"
		*/


		console.log(1 != true);//false
		/*
			With tyoe conversion: true was converted to 1
			1 is equal to 1 - it is not inequal.
		*/

		console.log(true != "true");// true
		// with type conversion: true was convtered to 1
		//"true" was converted into a number but the result is NaN
		//1 is not equal to NaN

		console.log("0" != false);//false
		//typer conversion: "0" converted to 0
		//false converted to 0
		//0 is equal to 0 - is NOT equal

	//Strict Inequality Operator (!==)
		console.log("5" !==5);//true

		// true - operands are inequal because they have different types

		console.log(5 !== 5);//false
		//false - operands are equal; they have the same value and the same type

		console.log("true" !== true);//true
		//true - operands are inequal because they have they have different types


	//Relationsl Comparison Operators
		// A comparison operator which will check the relationship between the operands

		let x = 500;
		let y = 700;
		let w = 8000;
		let numString3 = "5500";

		//Greater than (>)
		console.log(x>y);//false

		console.log(w>y);//true

		//Less Than (<)
		console.log(w<y);//false
		console.log(y<y);//false
		console.log(x < 1000);//true
		console.log(numString3 < 1000);//false - forced coercion to change the string into number
		console.log(numString3 < 6000);//true - forced coercion to number
		console.log(numString3 < "Jose");//true - "5500" < "Jose" - that is erratic

		//Greater than or Equal to (>=)
		console.log(w >= 10000);//false
		console.log(y >= x);//true

		//Less Than or Equal to (<=)
		console.log(x <= y);//true
		console.log(w <= x);//false

//Logical Operators - AND & OR
		//AND Operator(&&)
		//It means that both operands on the left and right or all operands added must be true or results to true.
		/*
			The left operand should be true, and the right operand should be true as well:
				T && T = TRUE
				F && T = FALSE
				T && F = FALSE
				F && F _ FALSE
		*/
		let isAdmin = false;
		let isRegistered = true;
		let isLegalAge = true;

		//to get authorization you should be an admin and registered

		let authorization1 = isAdmin && isRegistered;
		console.log(authorization1);//false - F && T

		//to get authorization you should be at legal age and registered

		let authorization2 = isLegalAge && isRegistered;
		console.log(authorization2);//true - T && T

		// to get authorization you should be an admin and is at legal age

		let authorization3 = isAdmin && isLegalAge;
		console.log(authorization3);//false - F && T

		let requiredLevel = 95;
		let requiredAge = 18;

		let authorization4 = isRegistered && requiredLevel === 25;
		console.log(authorization4);//false 

		let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
		console.log(authorization5);//true - T && T && T

		let userName1 = "gamer2001";
		let userName2 = "shadow1991"
		let userAge1 = 15;
		let userAge2 = 30;

		let registration1 = userName1.length > 8 && userAge1 >= requiredAge;
		// .length is a property of string which determines the number of characters in the string
		console.log(registration1);//false - T  && F - userAge1 does not meet the required age.

		let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
		console.log(registration2);//true - T && T - username2 length is greater than 8 and the required age is greater than 18 (the required age)

		// OR Operator (|| - Double Pipe)
			// OR operator returns true if at least one of the operands are true

			/*
				T || T = true
				F || T = true
				T || F = true
				F || F = false
			*/
			let userLevel1 = 100;
			let userLevel2 = 65;

			let guildRequirements = isRegistered && userLevel1 >= requiredLevel && userAge1 >= requiredAge;
			console.log(guildRequirements);//false - T && F


			let guildRequirements2 = isRegistered || userLevel1 >= requiredLevel || userAge1 >= requiredAge;
			console.log(guildRequirements);//true - T || F

			let guildRequirements3 = userLevel1 >= requiredLevel || userAge1 >= requiredAge;
			console.log(guildRequirements3);//true - T || F

			let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
			console.log(guildAdmin);//false - F || F

	// Not Operator (!)
		// turns the boolean into the opposite value
		/*
				T = F
				F = t
		*/

		let guildAdmin2 = !isAdmin || userLevel1 >= requiredLevel;
		console.log(guildAdmin2);//true - turned the value of isAdmin to true, hence, T || T

		console.log(!isAdmin);//true
		console.log(!isRegistered);//false


//Conditional Statements
		//if-else
				//if statement will run a block of code if the condition specified is true or results into true

				let userName3 = "crusader_1993";
				let userLevel3 = 25;
				let userAge3 = 20;

				/*if(true){
					alert("We just ran the if condition")
				};*/

				if(userName3.length > 10){
					console.log("Welcome to Game Online!")
				};

				if(userName3.length > 10 && isRegistered && isAdmin){
					console.log("Thank you for joining the Admin")
				};// does not execute since the condition is false


				//else statement will be run if the condition given is false or results to false

				if(userName3.length > 10 && isRegistered && isAdmin){
					console.log("Thank you for joining the Admin");}
					else{
						console.log("You are too strong to be noob. :(");
					};


			//else-if 
				// executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true.

				if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
					console.log("Thank you for joining the noobies guild");
				} else if(userLevel3 > 25){
					console.log("You too strong to be a noob")
				} else if(userAge3 > requiredAge){
					console.log("You are too young to join the guild.")
				} else if(userName3.length < 10){
					console.log("Username too short.")
				};

			//if-else in functions
				function addNum(num2, num2){
					if(typeof num1 === "number" && typeof num2 === "number"){
						console.log("Run only if both arguments passed are number type: ");
						console.log(num1 + num2);
					}else {
						console.log("One or both");
					}
				};
				addNum(5, 2);


				function login(username, password){
					//how can we check if the argument passed are strings?
					if(typeof username === 'string' && typeof	password === 'string'){
						console.log("Both arguments are strings.");
						
							/*nested if-else

								will run if the parent if statement is able to agree to accomplish its condition

								Mini-Activity

								Add another condition to our nested-if statement:
									- check if the password is at least 3 character long.

									add an else statement which runs if both conditions are not met:
										- show an alert whaich says
										"credentials too short."

										Stretch Goal:
										- add an else if statement that if the username is less than 8 characters long
											- show an alert "username is too short"

										- add an else if statement that if the password is less than 8 characters
											- show an alert "password is too short"*/
						
							if(password.length >= 3 && username.length >= 8){
								console.log("Awesome!");
						}else if(username.length < 8){
							alert("Username too short!");
						}else if(password.length < 8){
							alert("Password is too short!");
						}
					}else {
						console.log("One of the arguments is not a string!");
					}
				};
				login("will", "smith");



/*
		Mini-Activity:

			Create a function which will determine the color of shirt to wear depending on the day the user inputs.
					Monday - Black
					Tuesday - Green
					Wednesday - Yellow
					Thursday - Red
					Friday - Violet
					Saturday - Blue
					Sunday - White

				Log message on an alert window:
					"Today is <dayToday>, wear <colorOfTheDay>"

				If theday inout of range, log a message on an alert window:
					"Invalid inout. Enter a valid day of the week."

				Stretch Goal:
				Check if the argument passed is a string
					- log an alert if it is not:
						"Invalid inout. Please use input string"
					Research and use .toLowerCase(), so a user can inout in lowercase.

*/

function colorOfTheDay(day){
	if(typeof day === "string"){
		if(day.toLowerCase() === "monday"){
			alert(`Today if ${day}, wear Black.`);
		}else if(day.toLowerCase() === "tuesday"){
			alert(`Today is ${day}, wear Green.`);
		}else if(day.toLowerCase() === "wednedday"){
			alert(`Today is ${day}, wear Yellow.`);
		}else if(day.toLowerCase() === "thursday"){
			alert(`Today is ${day}, wear Red.`);
		}else if(day.toLowerCase() === "friday"){
			alert(`Today is ${day}, wear Violet.`);
		}else if(day.toLowerCase() === "saturday"){
			alert(`Today is ${day}, wear Blue.`);
		}else if(day.toLowerCase() === "sunday"){
			alert(`Today is ${day}, wear White.`);
		}
	}else{
		aler("Invalid input. Enter a valid day of the week.");
	}
}
colorOfTheDay("Monday");

/* Review:
		Mini-Activity
			- Name a variable appropriately. You may follow our guide in s14's note:

			Create a let variable with the name of your favorite food.

			create a let variable with the sum of 150 and 9

			create a let variable with the product of 100 and 90

			create a let variable with a boolean value.
				- this variable asks if the user is active

			create group of data with the names of your 5 favorite restaurants.

			create a variable which describes your favorite musical artist/singer or actor/ actresses with the following key: value pairs:

				firstName: <value>
				lastName: <value>
				stageName: <value>
				birthday: <value>
				age: <value>
				bestAlbum: <value>/bestTVShow: <value>
				bestSong: <value>/bestMovie: <value>
				isActive: <value>

			if the data is unavailable or there is actually NO value, add null

			Log all variables in the console

			create a function able to receive two umbers as aruments.
				- display the quotuent of the two numbers in the console
				- return the value of the division

			create a variable called quotient
				- this variable should be able yo receive the result of the division function.

			Log the quotient variable in the console with the following message:
				"The result of the division is : <valueOfQuotient>"

				- use concatenation/template literal
					`${variable}`

			Take a screenshot of your console and code base and send it in the hangouts.

*/

let favoriteFood = "Pizza";
let sum = 150 + 9;
let product = 100 * 90;
let isActive = true;
let favoriteRestaurants = ["Kempinski", "Villa Rosa", "Mugg & Bean", "McDonalds", "Chik-fil-A"];
console.log(favoriteRestaurants);
console.log(favoriteFood);
console.log(sum);
console.log(product);
console.log(isActive);

let actor = {
	firstName: "Dwayne",
	lastName: "Johnson",
	stageName: "The Rock",
	birthday: "2 May 1972",
	age: 49,
	bestTVShow: null,
	bestMovie: "G.I. Joe - Retaliation",
	isActive: true

};
console.log(actor);

function division(x,y) {
	return x/y;
};

let quotient = division(36,9);
console.log(`The result of the division is : ${quotient}`);

/*Switch Statements
		- is an alternative to an if, else-if, else tree. Wehere the data being evaluated or checked is of an expected inout.
		- used to select one of many code blocks or statements to be executed.
		- will compare your expression/condition to match with a case. If a case matches the given expression or condition, then the statement for that case will run/execute. 
			The switch expression and your case is executed to match. If it doesn't match, we will check for the next cases, if no cases match, the expression/condition, then the default code block will be executed. If we do have match, then the statement for that case will be run or executed. 

		Syntax:

		switch(expression/condition){
				case value:
							stetement;
							break;
				case value:
							statement;
							break;
				default:
							statement;
							break
		}

*/

let hero = "Hercules";

switch(hero){
	case "Jose Rizal":
		console.log("National Hero of the Phillipines.");
		//break terminates the code block. If this was not added to your case then the next code block will run as well. 
		break;
	case "George Washington":
		console.log("Hero of American Revolution.");
		break;
	case "Hercules":
		console.log("Legendary Hero of the Greek");
		break;
	default:
		console.log("Invalid input.");
		break;
};


/*Ternary Operators
		- a shorthand way of writing if-else statements

		Syntax:
				condition ? - the question mark represents the if-statement and the colon (:) represents the else statement
*/

let price = 5000;

/*if(price > 1000){
	console.log("Price is over 1000");
}else{
	console.log("Price is less than 1000");
}*/

// the above code can be rewritten in ternary form like:

price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000");

let hero2 = "Goku";

hero2 === "Vegeta" ? console.log("You are the prince of all Saiyans") : console.log("You are not even royalty."); //ternary operator. Also the else statements in a ternary operator is required

/*Ternary operators were created so that we can have an if-else statement in one line*/

villain === "Tow Face"
? console.log("You lived long enough to be a villain")
: console.log("Not quite villainous yet.");

/*Ternary operators are not meant for complex if-else trees. However, its advantage with our regular if-else, is not that it's short but also, ternary operation implicitly returns or it can return without the return keyword*/

let robin1 = "Dick Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin1 ? true : false;
console.log(isFirstRobin);

//else if with ternary operator
let a = 7;
a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10"): console.log("A is not 5 or 10"));